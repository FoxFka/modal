let selectedAddress = {}

class Modal {
  constructor(className) {
    this.overlay = document.querySelector(className);
    this.modal = this.overlay.querySelector('.modal');
    const initControls = document.querySelectorAll(`[data-modal="${className}"]`);
    for (let elem of initControls) {
      elem.onclick = (e) => {
        e.preventDefault();
        this.showModal();
      }
    }

    this.overlay.addEventListener('click', (e) => {
      e.preventDefault();
      if (e.target.classList.contains('js_modal-close')) {
        this.hideModal();
      }
    })
  }

  showModal() {
    this.overlay.classList.add('overlay_animated');
    setTimeout(() => {
      this.overlay.classList.add('overlay_show');
      this.modal.classList.add('modal_show');
    }, 5);
    setTimeout(() => this.overlay.classList.remove('overlay_animated'), 205);
  }

  hideModal() {
    this.overlay.classList.add('overlay_animated');
    this.overlay.classList.remove('overlay_show');
    this.modal.classList.remove('modal_show');
    setTimeout(() => this.overlay.classList.remove('overlay_animated'), 200);
  }
}

const addressModal = new Modal('.js_address-modal')

const addressPicker = new autoComplete({
  selector: 'input[name="address"]',
  offsetLeft: -8,
  offsetTop: -8,
  minChars: 2,
  async source(term, response) {
    const suggestions = await fetch('https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address', {
    	method: 'POST',
    	headers: {
    		'Content-Type': 'application/json',
    		'Accept': 'application/json',
    		'Authorization': 'Token 235a47774c97753d9b7bfca1bfe0ce3ffcbfdff8'
    	},
    	body: JSON.stringify({
    		query: term
    	})
    })
    if (suggestions.ok) {
      const json = await suggestions.json()
      response(json.suggestions)
    } else {
      response(['Не удалось получить ответ от сервера'])
    }
  },
  renderItem(item) {
    return `<div data-info='${JSON.stringify(item)}' class="autocomplete-suggestion" title="${item.value}" data-val="${item.value}">${item.value}</div>`;
  },
  onSelect(event, term, item) {
    selectedAddress = JSON.parse(item.dataset.info);
  }
})

document.querySelector('.js_change-address').addEventListener('click', function () {
  addressModal.hideModal();
  setAddressData(selectedAddress);
});

function setAddressData (addressData) {
  document.querySelector('.js_selected-address-data').innerHTML = `Информация о выбранном адресе: <pre>${JSON.stringify(addressData, null, '  ')}</pre>`;
}